Framing is intended to be a generic tool for use by custom frame shops.

It will perform calculations such as:
- United Inches based on size of artwork (with and without mats)
- amount of moulding required for a specific project
- total project cost (these values will be determined by users in the
  'price-sheet' file, as price sheets contain proprietary information and are
  different from shop to shop.)