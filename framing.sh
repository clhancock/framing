#! /bin/bash

#Part 1: Calculate United Inches
	read -p "Art width : " art_width
	read -p "Art height : " art_height

	#Input art dimensions
	echo "Art width = $art_width"
	echo "Art height = $art_height"
	#Calculate UI without mats and print.
	art_ui=$(awk "BEGIN {print $art_width+$art_height; exit}")
	echo "art UI = $art_ui"

	#Input mat margins...
	read -p "Left mat margin : " mat_left
	read -p "Right mat margin : " mat_right
	read -p "Top mat margin : " mat_top
	read -p "Bottom mat margin : " mat_bottom
	#...and calculate UI of the Mats
	mat_ui=$(awk "BEGIN {print $mat_left+$mat_right+$mat_top+$mat_bottom; exit}")
	echo "mat UI = $mat_ui"

	#Add together the Art and Mat UIs to get the total United Inches
	tot_ui=$(awk "BEGIN {print $mat_ui+$art_ui; exit}")
	echo "total UI = $tot_ui"

# Part 2: Calculate the length of wood needed
# This will probably require looking up values from a table, as well as rounding up previous calculations
# to nearest available value
